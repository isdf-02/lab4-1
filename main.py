"""开发画图软件"""
# 导入需要用到的tkinter库
from tkinter import *
from tkinter.colorchooser import *
from abc import ABC, abstractmethod


# 抽象工厂
class ShapeFactory(ABC):
    @abstractmethod
    def createDwgObj(self, drawpad):
        pass

# 具体生产直线的工厂
class LineFactory(ShapeFactory):
    # 重写父类抽象工厂的方法，并返回工厂需要生产的对象——直线对象
    def createDwgObj(self, drawpad):
        return myline(drawpad)

class DrawingTool():
    def __init__(self, drawpad):
        self.fgColor = "#ff0000"    # 作图颜色
        self.bgColor = "#000000"    # 背景颜色
        self.drawpad = drawpad      # 画板属性
        self.startDrawFlag = False  # 开始画图标志, false表示未开始绘图
        self.lastDraw = 0   # 记录最后一次绘图的id
        self.x = 0  # 绘图开始坐标x
        self.y = 0  # 绘图开始坐标y

    # 开始绘图
    def startDraw(self, event, drawpad):
        drawpad.delete(self.lastDraw)   # 清除上次绘图记录
        if not self.startDrawFlag:  # 若画图标志为flase
            self.startDrawFlag = True   # 置画图标志为true
            self.x = event.x    # 将鼠标当前点击的x坐标作为起点x坐标
            self.y = event.y    # 将鼠标当前点击的y坐标作为起点y坐标

    # 结束绘图
    def stopDraw(self):
        self.startDrawFlag = False  # 画图标志置为flase
        self.lastDraw = 0   # 绘图记录清零

class myline(DrawingTool):  # 继承DrawingTool基类
    def __init__(self, drawpad):
        super().__init__(drawpad)   # 初始化调用父类构造器，并传入相应参数

    # 实现具体的直线画法
    def drawLine(self, event):
        # 先开启画图
        self.startDraw(event, self.drawpad)
        # 调用tk库中canvas类的方法creat_something
        # 参数为起点坐标、终点坐标、绘图颜色, 还有其它参数可选填
        self.lastDraw = self.drawpad.create_line(self.x, self.y, event.x, event.y, fill=self.fgColor)

class mylineArrow(DrawingTool):
    def __init__(self, drawpad):
        super().__init__(drawpad)

    def drawLineArrow(self, event):
        self.startDraw(event, self.drawpad)
        self.lastDraw = self.drawpad.create_line(self.x, self.y, event.x, event.y, arrow=LAST, fill=self.fgColor)

class myRect(DrawingTool):
    def __init__(self, drawpad):
        super().__init__(drawpad)

    def drawRect(self, event):
        self.startDraw(event, self.drawpad)
        self.lastDraw = self.drawpad.create_rectangle(self.x, self.y, event.x, event.y, outline=self.fgColor)

class myPen(DrawingTool):
    def __init__(self, drawpad):
        super().__init__(drawpad)

    def drawByPen(self, event):
        self.startDraw(event, self.drawpad)
        self.drawpad.create_line(self.x, self.y, event.x, event.y, fill=self.fgColor)
        self.x = event.x
        self.y = event.y

class myEraser(DrawingTool):
    def __init__(self, drawpad):
        super().__init__(drawpad)

    def ereaseDraw(self, event):
        self.startDraw(event, self.drawpad)
        self.drawpad.create_rectangle(event.x - 8, event.y - 8, event.x + 8, event.y + 8, fill=self.bgColor)
        self.x = event.x
        self.y = event.y


# 这是主程序，主要负责UI创建和事件处理
# 继承于tk库中的Frame类
class Application(Frame):
    def __init__(self, master=None, bgColor="#000000"):
        super().__init__(master)  # super()代表的是父类的定义, 而不是父类对象
        self.master = master
        self.bgColor = bgColor
        self.fgColor = "#ff0000"
        self.drawpad = NONE    # 画布初始化为NONE, 我们在后续方法中创建
        self.pack()    # pack()方法是tk库中的一种布局管理器, 具体使用见后面的方法
        self.createWidget()    # 初始化时就创建UI界面
        # 接下来创建工具类对象属性
        # 先创建直线工厂
        self.linefactory = LineFactory()
        # 通过直线工厂生产直线对象
        self.mLine = self.linefactory.createDwgObj(self.drawpad)
        self.mEraser = myEraser(self.drawpad)
        self.mLineArrow = mylineArrow(self.drawpad)
        self.mRect = myRect(self.drawpad)
        self.mPen = myPen(self.drawpad)

    def createWidget(self):
        # 创建绘图区域
        self.drawpad = Canvas(root, width=win_width, height=win_height * 0.9, bg=self.bgColor)
        self.drawpad.pack()
 
        # 创建按钮
        btn_start = Button(root, text="开始", name="start")
        btn_start.pack(side="left", padx="10")  # 按钮靠左排放, 水平空白边距10
 
        btn_pen = Button(root, text="画笔", name="pen")
        btn_pen.pack(side="left", padx="10")
 
        btn_rect = Button(root, text="矩形", name="rect")
        btn_rect.pack(side="left", padx="10")
 
        btn_clear = Button(root, text="清屏", name="clear")
        btn_clear.pack(side="left", padx="10")
 
        btn_erasor = Button(root, text="橡皮擦", name="eraser")
        btn_erasor.pack(side="left", padx="10")

        btn_line = Button(root, text="直线", name="line")
        btn_line.pack(side="left", padx="10")

        btn_lineArrow = Button(root, text="箭头直线", name="lineArrow")
        btn_lineArrow.pack(side="left", padx="10")
 
        btn_color = Button(root, text="颜色", name="color")
        btn_color.pack(side="left", padx="10")
 
        # 事件处理
        # 将按钮类的点击事件都和事件处理方法绑定
        btn_pen.bind_class("Button", "<1>", self.eventManager)
        # 将画布中鼠标释放事件和停止画图方法绑定
        self.drawpad.bind("<ButtonRelease-1>", self.stopDraw)

    def eventManager(self, event):  # 事件处理方法, 统一事件处理
        name = event.widget.winfo_name()    # 获取事件名称
        print(name)
        if name == "line":  # 按下直线按钮
            # 在画布中将鼠标按下移动事件和属性中直线对象的画图方法绑定
            self.drawpad.bind("<B1-Motion>", self.mLine.drawLine)
        elif name == "lineArrow":
            self.drawpad.bind("<B1-Motion>", self.mLineArrow.drawLineArrow)
        elif name == "rect":
            self.drawpad.bind("<B1-Motion>", self.mRect.drawRect)
        elif name == "pen":
            self.drawpad.bind("<B1-Motion>", self.mPen.drawByPen)
        elif name == "eraser":
            self.drawpad.bind("<B1-Motion>", self.mEraser.ereaseDraw)
        elif name == "clear":
            self.drawpad.delete("all")
        elif name == "color":
            c = askcolor(color=self.fgColor, title="选择画笔颜色")
            # [(255,0,0),"#ff0000"]
            self.fgColor = c[1]

    def stopDraw(self, event):
        # 分别调用作图工具的停止画图方法
        self.mLine.stopDraw()
        self.mLineArrow.stopDraw()
        self.mRect.stopDraw()
        self.mPen.stopDraw()
        self.mEraser.stopDraw()
 
 
if __name__ == '__main__':
    # 窗口的宽度和高度
    win_width = 900
    win_height = 450
    # 建立一个Tk类对象
    root = Tk()
    root.geometry(str(win_width) + "x" + str(win_height) + "+200+300")
    root.title("画图软件")  # 给软件起名字
    Application(master=root)
    # 让程序进入无限循环中一直运行, 直到我们手动关闭
    root.mainloop()